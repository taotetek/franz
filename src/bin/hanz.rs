use zmq;

fn main() {

    let zmq_addr = "tcp://127.0.0.1:31339";
    let zmq_ctx = zmq::Context::new();
    let socket = zmq_ctx.socket(zmq::SUB).unwrap();
    socket.set_subscribe(b"kernel").unwrap();
    socket.connect(&zmq_addr).expect("could not connect");

    loop {
        let _topic = socket.recv_msg(0).unwrap();
        let data = socket.recv_msg(0).unwrap();
        println!("{}", std::str::from_utf8(&data).unwrap());
    }
}

