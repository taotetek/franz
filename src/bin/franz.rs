extern crate futures;
extern crate rdkafka;
extern crate rdkafka_sys;
extern crate serde;

use futures::stream::Stream;
use rdkafka::client::ClientContext;
use rdkafka::config::ClientConfig;
use rdkafka::consumer::stream_consumer::StreamConsumer;
use rdkafka::consumer::{Consumer, ConsumerContext, Rebalance};
use rdkafka::error::KafkaResult;
use rdkafka::message::Message;
use serde::{Serialize, Deserialize};
use zmq;
use std::env;

#[derive(Serialize, Deserialize, Debug)]
struct LogMeta {
    syslog_program: String,
    syslog_host: String,
    noidx_rawmsg: String,
}

// Set up Kafka Consumer
struct CustomContext;
impl ClientContext for CustomContext {}
impl ConsumerContext for CustomContext {
    fn pre_rebalance(&self, rebalance: &Rebalance) {
        println!("Pre rebalance {:?}", rebalance);
    }

    fn post_rebalance(&self, rebalance: &Rebalance) {
        println!("Post rebalance {:?}", rebalance);
    }

    fn commit_callback(
        &self,
        result: KafkaResult<()>,
        _offsets: *mut rdkafka_sys::RDKafkaTopicPartitionList,
    ) {
        println!("Committing offsets: {:?}", result);
    }
}

type LoggingConsumer = StreamConsumer<CustomContext>;

fn main() -> Result<(), failure::Error> {

    // Get ENV vars
    let kafka_brokers = match env::var("FRANZ_KAFKA_BROKERS") {
        Ok(brokers) => brokers,
        Err(_) => return Err(failure::format_err!("FRANZ_KAFKA_BROKERS must be set"))
    };

    let kafka_topic = match env::var("FRANZ_KAFKA_TOPIC") {
        Ok(topic) => topic,
        Err(_) => return Err(failure::format_err!("FRANZ_KAFKA_TOPIC must be set"))
    };

    let zmq_addr = match env::var("FRANZ_ZMQ_ADDRESS") {
        Ok(addr) => addr,
        Err(_) => return Err(failure::format_err!("FRANZ_ZMQ_ADDRESS must be set"))
    };

    // Set up ZMQ publisher
    let zmq_ctx = zmq::Context::new();
    let socket = zmq_ctx.socket(zmq::PUB)?;
    socket.bind(&zmq_addr)?;

    // Set up Kafka consumer
    let context = CustomContext;
    let consumer: LoggingConsumer = ClientConfig::new()
        .set("group.id", "rusttest")
        .set("bootstrap.servers", &kafka_brokers)
        .set("enable.auto.commit", "false")
        .set("session.timeout.ms", "6000")
        .create_with_context(context)?;

    consumer.subscribe(&[&kafka_topic])?;

    // start and consume from kafka 
    let message_stream = consumer.start();
    for message in message_stream.wait() {
        match message {
            Err(_) => { return Err(failure::format_err!("message_stream.wait() error")); }
            Ok(Err(e)) => return Err(failure::format_err!("error: {}", e)),
            Ok(Ok(m)) => {
                let payload = match m.payload_view::<str>() {
                    None => "",
                    Some(Ok(s)) => s,
                    Some(Err(e)) => return Err(failure::format_err!("kafka error wtf: {}", e)),
                };
                let log_meta: LogMeta = serde_json::from_str(&payload)?;
                println!("{}", payload);
                socket.send(&log_meta.syslog_program, zmq::SNDMORE)?;
                socket.send(&log_meta.noidx_rawmsg, 0)?;
            }
        };
    }
    Ok(())
}
