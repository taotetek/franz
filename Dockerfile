FROM rust@sha256:c52a5038fe5da4ee5d454b2294a297659ccabbf4419894b5bc91d2cd7817b367 as build
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install libzmq3-dev libclang-dev clang build-essential && rm -rf /var/lib/apt/lists/*

COPY ./ ./

RUN cargo build --release

RUN mkdir -p /build-out

RUN cp target/release/franz /build-out/

# Ubuntu 18.04
FROM ubuntu@sha256:9b1702dcfe32c873a770a32cfd306dd7fc1c4fd134adfb783db68defc8894b3c

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install ca-certificates libssl-dev libzmq5 && rm -rf /var/lib/apt/lists/*

COPY --from=build /build-out/franz /

CMD /franz
